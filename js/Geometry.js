var Geometry = {};

Geometry.polarToCartesian = function (r, phi) {
  return {x: r * Math.cos(phi), y: r * Math.sin(phi)};
};

Geometry.cartesianToAngle = function(p) {
  var phi = Math.atan2(p.y, p.x);

  if(phi < -Math.PI / 2) {
    return phi + Math.PI * 2;
  }

  return phi;
};

Geometry.translate = function (p, v) {
  return {x: p.x + v.x, y: p.y + v.y};
};

Geometry.degToRad = function (angle) {
  return angle * Math.PI / 180;
};

Geometry.radToDeg = function(angle) {
  return angle * 180 / Math.PI;
};