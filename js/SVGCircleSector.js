function SVGCircleSector(center, radius, startAngle, endAngle) {
  var largeArcFlag = +(endAngle - startAngle >= 180),
      path = document.createElementNS(this.SVG_NS, "path"),
      start,
      end;

  this.center = center;
  this.radius = radius;
  this.startAngle = startAngle;
  this.endAngle = endAngle;

  start = this.angleToPoint(startAngle);
  end = this.angleToPoint(endAngle);

  path.pathSegList.appendItem(path.createSVGPathSegMovetoAbs(center.x, center.y));
  path.pathSegList.appendItem(path.createSVGPathSegLinetoAbs(start.x, start.y));
  path.pathSegList.appendItem(path.createSVGPathSegArcAbs(end.x, end.y, radius, radius, 0, largeArcFlag, 1));
  path.pathSegList.appendItem(path.createSVGPathSegClosePath());

  this.path = path;
}

SVGCircleSector.prototype.SVG_NS = 'http://www.w3.org/2000/svg';

SVGCircleSector.prototype.updateEndAngle = function (endAngle) {

  var end = this.angleToPoint(endAngle),
      largeArcFlag = +(endAngle - this.startAngle >= 180);

  this.path.pathSegList.replaceItem(
      this.path.createSVGPathSegArcAbs(end.x, end.y, this.radius, this.radius, 0, largeArcFlag, 1),
      2);

  this.endAngle = endAngle;

  this.onupdate && this.onupdate();
};

// All angles start in fact from -90 degrees, the topmost point of the circle

SVGCircleSector.prototype.angleToPoint = function (angle) {
  return Geometry.translate(
      Geometry.polarToCartesian(this.radius, Geometry.degToRad(angle - 90)),
      this.center
      );
};

SVGCircleSector.prototype.pointToAngle = function (point) {
  return Geometry.radToDeg(
      Geometry.cartesianToAngle(
          Geometry.translate(point, {
            x: -this.center.x,
            y: -this.center.y
          }))) + 90;
};

SVGCircleSector.prototype.onupdate = null;
