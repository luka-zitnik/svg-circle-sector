function setUpClockEvents(clock) {

  var mousedown = false,
      intervalID;

  document.getElementById("play-btn").onclick = function () {

    window.clearInterval(intervalID);

    intervalID = window.setInterval(function () {

      if (clock.endAngle >= 360) {
        window.clearInterval(intervalID);
        return;
      }

      clock.updateEndAngle(clock.endAngle + 1);
    }, 10);
  };

  document.getElementById("pause-btn").onclick = function () {
    window.clearInterval(intervalID);
  };

  document.getElementById("reset-btn").onclick = function () {
    window.clearInterval(intervalID);
    clock.updateEndAngle(0);
  };

  clock.path.ownerSVGElement.addEventListener("mousedown", function (event) {
    event.preventDefault(); // Prevents the dragstart event
    mousedown = true;
  });

  clock.path.ownerSVGElement.addEventListener("mousemove", function (event) {
    var ctm,
        screenPoint,
        svgPoint;

    if (mousedown) {

      screenPoint = event.currentTarget.createSVGPoint();
      screenPoint.x = event.clientX;
      screenPoint.y = event.clientY;

      ctm = event.currentTarget.getScreenCTM();

      svgPoint = screenPoint.matrixTransform(ctm.inverse());

      clock.updateEndAngle(parseInt(clock.pointToAngle(svgPoint)));
    }
  });

  clock.path.ownerSVGElement.addEventListener("mouseup", function (event) {
    mousedown = false;
  });
}

function drawClock () {

  var ANGLES = [45, 270, 360],
      MASK_RADIUS = 13,
      PHASES_RADIUS = 20,
      CLOCK_RADIUS = 50,
      CENTER = {x: 50, y: 50};

  var fragment = document.createDocumentFragment(),
      clock = new SVGCircleSector(CENTER, CLOCK_RADIUS, 0, 0),
      text = SVGElementsGenerator.createSVGText(CENTER.x, CENTER.y),
      i = 0,
      phases = [];

  fragment.appendChild(clock.path);

  for (; i < ANGLES.length; ++i) {
    fragment.appendChild((phases[i] = new SVGCircleSector(
        CENTER, PHASES_RADIUS, ANGLES[i - 1] || 0, ANGLES[i]
        ).path));
  }

  // This should be a mask instead
  fragment.appendChild(SVGElementsGenerator.createSVGWhiteCircle(CENTER, MASK_RADIUS));

  fragment.appendChild(text);

  document.getElementById("clock").appendChild(fragment);

  setUpClockEvents(clock);

  clock.onupdate = function () {

    var i;

    text.textContent = this.endAngle;

    for (i = 0; i < ANGLES.length; ++i) {
      phases[i].classList.remove("current");
    }

    for (i = 0; i < ANGLES.length; ++i) {
      if (this.endAngle < ANGLES[i]) {
        phases[i].classList.add("current");
        break;
      }
    }
  };
}

drawClock();
