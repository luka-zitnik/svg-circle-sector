var SVGElementsGenerator = {};

SVGElementsGenerator.SVG_NS = "http://www.w3.org/2000/svg";

SVGElementsGenerator.createSVGCircle = function (center, radius) {
  var circle = document.createElementNS(this.SVG_NS, "circle");

  circle.setAttributeNS(null, "cx", center.x);
  circle.setAttributeNS(null, "cy", center.y);
  circle.setAttributeNS(null, "r", radius);

  return circle;
};

SVGElementsGenerator.createSVGWhiteCircle = function (center, radius) {
  var circle = this.createSVGCircle(center, radius);
  circle.classList.add("white");
  return circle;
};

SVGElementsGenerator.createSVGText = function (x, y) {
  var text = document.createElementNS(this.SVG_NS, "text");
  text.setAttributeNS(null, "x", x);
  text.setAttributeNS(null, "y", y);
  return text;
};